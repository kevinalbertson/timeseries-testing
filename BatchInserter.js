function BatchInserter(batchSize, collection) {
    var currentBatch;

    function insert(doc) {
        if (currentBatch.docs.length == batchSize) {
            flush();
            _reset();
        }
        currentBatch.docs.push(doc);
    }

    function flush() {
        collection.insert(currentBatch);
    }

    function _reset() {
        currentBatch = {
            docs: []
        };
    }

    _reset();

    return {
        flush: flush,
        insert: insert
    };
}