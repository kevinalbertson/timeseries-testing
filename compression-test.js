// Make collections comparing server status and random small docs.
load("utils.js");
load("delta.js");
load("BatchInserter.js");

(function() {
    "use strict";

    var compressionType = "zlib";
    Random.setRandomSeed();
    var summary = "Test Case\tNumber of Docs\tBatch Size\tCompression\tBatch Collection Size\tBatch Collection Storage Size\t" +
            "Unbatch Collection Size\tUnbatch Collection Storage Size\tStorage Size Ratio\tBatch Collection\tUnbatch Collection\n";

    function _record(testcase, nDocs, batchSize, compression, batchColl, unbatchColl) {
        // Validate collections to get accurate size results.
        db.fsyncLock();

        var batchStats = batchColl.stats();
        var unbatchStats = unbatchColl.stats();
        var rowStr = [testcase, nDocs, batchSize, compression, batchStats.size, batchStats.storageSize,
            unbatchStats.size, unbatchStats.storageSize, unbatchStats.storageSize / batchStats.storageSize,
            batchColl.getFullName(), unbatchColl.getFullName()].join("\t");
        summary += rowStr + "\n";
        print(rowStr);

        db.fsyncUnlock();
    }

    function _geoPt(lat, lng) {
        return {
            "type": "Point",
            "coordinates": [lat, lng]
        };
    }

    function _genRandomDoc() {
        return {
            rand_num: Random.srand(),
            rand_str: generateRandomString(256),
        };
    }

    function _genLiveServerStatus() {
        return db.serverStatus();
    }

    // Docs generated have lots of equal fields.
    function _genRandomDocWithSimilarity() {
        return {
            rand_num: Random.srand(),
            rand_str: generateRandomString(256),
            same_str: ["lorem", "ipsum", "dolor"][Random.randInt(3)],
            same_num: [1, 2, 3][Random.randInt(3)],
            same_obj: [_geoPt(0,0), _geoPt(1,0), _geoPt(0,1)][Random.randInt(3)]
        };
    }

    function runCompressionTest(testcase, docGenerator, nDocs, batchSize) {
        var batchColl = db["batch" + testcase];
        var unbatchColl = db["unbatch" + testcase];
        var batchBulk = batchColl.initializeUnorderedBulkOp();
        var unbatchBulk = unbatchColl.initializeUnorderedBulkOp();
        var batchInserter = new BatchInserter(batchSize, batchBulk);
        batchColl.drop();
        unbatchColl.drop();
        var tenPerc = Math.floor(nDocs / 10);
        for (var i = 0; i < nDocs; i++) {
            var doc = docGenerator();
            batchInserter.insert(doc);
            unbatchBulk.insert(doc);
            if (i % tenPerc == 0) {
                print("-" + i + "/" + nDocs + "-");
                batchInserter.flush();
                batchBulk.execute();
                batchBulk = batchColl.initializeUnorderedBulkOp();
                batchInserter = new BatchInserter(batchSize, batchBulk);
            }
        }
        batchInserter.flush();
        batchBulk.execute();
        unbatchBulk.execute();
        _record(testcase, nDocs, batchSize, compressionType, batchColl, unbatchColl);
    }

    function runCompressionTestFromCollection(testcase, batchSize, coll, useDelta) {
        var batchColl = db["batch" + testcase];
        batchColl.drop();
        var batchBulk = batchColl.initializeUnorderedBulkOp();
        var batchInserter = new BatchInserter(batchSize, batchBulk);
        
        var nDocs = coll.count();
        var tenPerc = Math.floor(nDocs / 10);
        var cursor = coll.find();

        var prevDoc = null;
        for (var i = 0; i < nDocs; i++) {
            var doc = cursor.next();
            if (useDelta) {
                if (prevDoc) {
                    var tempDoc = doc;
                    doc = simpleDelta(prevDoc, doc);
                    printjson(doc);
                    prevDoc = tempDoc;
                } else {
                    prevDoc = doc;
                }
            }
            batchInserter.insert(doc);
            if (i % tenPerc == 0) {
                print("-" + i + "/" + nDocs + "-");
                batchInserter.flush();
                batchBulk.execute();
                batchBulk = batchColl.initializeUnorderedBulkOp();
                batchInserter = new BatchInserter(batchSize, batchBulk);
            }
        }
        batchInserter.flush();
        batchBulk.execute();
        _record(testcase, nDocs, batchSize, compressionType, batchColl, coll);
    }

    runCompressionTestFromCollection("from_ftdc", 100, db.ftdc, true);
    // runCompressionTest("serverstatus_live", _genLiveServerStatus, 1000, 100);
    // runCompressionTest("random", _genRandomDoc, 1000, 100);
    // runCompressionTest("similar", _genRandomDocWithSimilarity, 1000, 100);

    print("Finished all tests\n");
    print(summary);

}());