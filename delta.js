// Recursively computes diff between two Javascript objects.
// Arrays are not recursed currently.
// Assumes that obj1 has a subset of keys of obj2. If not, then original objects are not reconstructbile.
function simpleDelta(obj1, obj2) {
    function isEmpty(obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) return false;
        }
        return true;
    }

    var diff = {};
    for (var prop in obj2) {
        if (!obj2.hasOwnProperty(prop)) continue;
        if (!obj1.hasOwnProperty(prop) || obj1[prop] !== obj2[prop]) {
            if (typeof(obj1[prop]) == "object" && !(obj1[prop] instanceof ObjectId)) {
                var delta = simpleDelta(obj1[prop], obj2[prop]);
                if (!isEmpty(delta)) {
                    diff[prop] = delta;
                }
            } else {
                diff[prop] = obj2[prop];
            }
        }
    }
    return diff;
}